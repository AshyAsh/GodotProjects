extends Node

func _ready():
	# Prints hello world to the output console
	print("Hello World!")
	
	# The course I am enroled in, made a challenge where you
	# had to create a variable and print "What are you waiting for? GODOT"
	# in the output console as well
	var challenge = "What are we waiting for? GODOT!"
	print(challenge)
