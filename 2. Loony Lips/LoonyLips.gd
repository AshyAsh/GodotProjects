extends Control

var playerWords = []
var currentStory = {}

onready var playerText = $VBoxContainer/HBoxContainer/PlayerText
onready var displayText = $VBoxContainer/DisplayText
onready var okayLabel = $VBoxContainer/HBoxContainer/Label

func _ready():
	pickCurrentStory()
	displayText.text = "Welcome to LoonyLips. This is a story generator \"game\". "
	checkPlayerWordsLength()
	playerText.grab_focus()

func pickCurrentStory():
	randomize()
	var stories = $StoryBook.get_child_count()
	var selectedStory = randi() % stories
	currentStory.prompts = $StoryBook.get_child(selectedStory).prompts
	currentStory.story = $StoryBook.get_child(selectedStory).story

func _on_PlayerText_text_entered(new_text):
	addToPlayerWords()

func _on_TextureButton2_pressed():
	if isStoryDone():
		get_tree().reload_current_scene()
	else:
		addToPlayerWords()

func addToPlayerWords():
	playerWords.append(playerText.text)
	displayText.text = ""
	playerText.clear()
	checkPlayerWordsLength()

func isStoryDone():
	return playerWords.size() == currentStory.prompts.size()

func checkPlayerWordsLength():
	if isStoryDone():
		endGame()
	else:
		promptPlayer()

func tellStory():
	displayText.text = currentStory.story % playerWords
	
func promptPlayer():
	displayText.text += "May I have " + currentStory.prompts[playerWords.size()] + ", please?"

func endGame():
	playerText.queue_free()
	tellStory()
	
	okayLabel.text = "Again!"
